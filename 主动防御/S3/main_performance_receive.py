#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 19-5-26 21:25
# @Author  : JSQ
# @File    : main_performance_receive.py
# @Software: PyCharm
# 传输文件服务端,监听并接收来自三台服务器的性能上报数据

import os
import json
import struct
import socketserver
import traceback
import hmac

#认证客户端链接
def conn_auth(conn):
    print('开始验证客户端发起的新连接的合法性:')
    msg=os.urandom(32)
    conn.sendall(msg)
    respone = conn.recv(16)
    with open(os.getcwd() + '/validusers.txt', 'r') as f:        # validusers.txt存放了内网合法的用户mac地址
        for line in f.readlines():
            secret_key = str.encode(line.strip())
            h = hmac.new(secret_key, msg)
            digest = h.digest()
            if hmac.compare_digest(respone,digest):
                return True
        return False
        f.close()

def data_handler(conn,bufsize=1024):
    if not conn_auth(conn):
        print('该连接不合法,关闭!')
        conn.close()
        return
    print('连接合法,开始通信!')
    conn.sendall(b'1')

#接收文件并保存
def recv_file(head_dir, tcp_client):
    buffsize = 1024
    filename = head_dir['filename']
    filesize_b = head_dir['filesize_bytes']
    recv_len = 0
    recv_mesg = b''
    f = open(os.getcwd() + '/JsonPerformanceReport/' + filename, 'wb')
    while recv_len < filesize_b:
        if filesize_b - recv_len > buffsize:
            recv_mesg = tcp_client.recv(buffsize)
            recv_len += len(recv_mesg)
            f.write(recv_mesg)
        else:
            recv_mesg = tcp_client.recv(filesize_b - recv_len)
            recv_len += len(recv_mesg)
            f.write(recv_mesg)
    f.close()
    print('文件接收完成-->', filename)

#从BaseRequestHandler继承，并重写handle方法
class MyServer(socketserver.BaseRequestHandler):
    def handle(self):
        connection = self.request
        data_handler(connection)
        print("Connected by", connection)
        while True:                                                    #　循环监听（读取）来自客户端的数据
            try:
                struct_len = connection.recv(4)                        # 接受报头的长度
                struct_info_len = struct.unpack('i', struct_len)[0]    # 解析得到报头信息的长度
                head_info = connection.recv(struct_info_len)           # 接受报头的内容
                head_dir = json.loads(head_info.decode('utf-8'))       # 将报头的内容反序列化
                recv_file(head_dir, connection)
            except:
                break

def receive():
    HOST_S3 = '172.16.0.4'
    print("服务端开始监听Performance###############")
    s = socketserver.ThreadingTCPServer((HOST_S3, 8086), MyServer)     # 多线程监听8086端口
    s.serve_forever()                                                  # 服务器一直开着

if __name__ == '__main__':
    receive()
