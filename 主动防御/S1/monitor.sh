#!/bin/bash

#while true;do

    ip=$(cat conn.log | cut -f 4 -d ',')
    vm_sh=$(ps -ef | grep vm.sh | grep /bin/bash | tr -s [:blank:] | cut -f 2 -d ' ' | sed '2,$d')
    file_sh=$(ps -ef | grep watch.sh | grep /bin/bash | tr -s [:blank:] | cut -f 2 -d ' ' | sed '2,$d')
    #echo $ip, $vm_sh, $file_sh
    if [ !`grep '$ip' white_ip_list.txt` ];then
        echo "$ip is suspicious"
        echo "$ip" >> suspicious_ip_list.txt
        { if [ "$vm_sh" = "" ];then
            echo "开启虚拟机监听程序"
            ./vm.sh
        fi }&
        { if [ "$file_sh" = "" ];then
            echo "开启文件系统监听程序"
            ./watch.sh
        fi }&
    fi
    
    #cat /dev/null > conn.log

    ssh_con=$(netstat -ntp | grep ssh)
    #echo $ssh_con
    if [ "$ssh_con" = "" ];then
        echo "all suspicous ssh_connections are done"
        if [ "$vm_sh" != "" ];then
            echo "关闭虚拟机监听程序"
            kill -9 $vm_sh
        fi
        if [ "$file_sh" != "" ];then
            echo "关闭文件系统监听程序"
            kill -9 $file_sh
        fi
    fi

#done
