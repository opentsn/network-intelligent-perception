#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    :
# @Author  : GY
# @File    : read_filesystem.py
#采集主机行为数据，并按照规定数据格式，生成JSON文件

import os
import time
import json


def read_FileSystem():
    f = open("log//watch.log", "r")
    time = []
    directory = []
    file = []
    event = []
    while True:
        line = f.readline()
        if line:
            line = line.strip('\n')
            line = line.split(" ")
            time.append(" ".join(line[0:2]))
            directory.append(line[2])
            file.append(line[3])
            event.append(line[4])
        else:
            break
    return time, directory, file, event


def Write_FileSystem_json(time, directory, file, event):
    data_dir = {
        "msg": "report",
        "layer": "filesystem",
        "layer_info": {
            "FileEvent": [

            ],
        }
    }

    for i in range(len(time)):
        name = {}
        name["time"] = time[i]
        name["directory"] = directory[i]
        name["file"] = file[i]
        name["event"] = event[i]
        data_dir["layer_info"]["FileEvent"].append(name)

    # path = os.getcwd() + '/JsonFileReport/'  # 获取当前工程下的JsonFile目录
    # with open(path + 'report_host.json', "w") as f:
    with open('report_filesystem.json', "w") as f:
        json.dump(data_dir, f)
        print("*****文件系统监控信息写入json成功*****")
    f.close()
    return 0


def ReadFileSystem_WriteJson():
    time, directory, file, event = read_FileSystem()
    Write_FileSystem_json(time, directory, file, event)


if __name__ == '__main__':
    ReadFileSystem_WriteJson()
