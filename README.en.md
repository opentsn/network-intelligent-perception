# Network Intelligent Perception and Awareness

#### SPECIAL THANKS and ACKNOWLEDGEMENT TO

Special thanks to the OpneTSN project team for their platform support, technical support, and management support for the open source project related to "Network Intelligence Awareness and Awareness".

#### Description
The code in the warehouse is mainly divided into five major parts:


- Encrypted traffic analysis
- Anonymous communication traffic analysis
- Next generation network processor
- Network measurement
- Active Defense

Contribution source of code in this warehouse: School of Cyberspace Science and Engineering, Southeast University

Code contribution team in the warehouse: Network Intelligence Perception and Awareness Laboratory, School of Cyberspace Science and Engineering, Southeast University


Teachers of the code contribution team in the warehouse: Professor Cheng Guang, Associate Professor Wu Hua, Associate Professor Hu Xiaoyan, Zhishan Postdoctoral Fellow Zhou Yuyang, Mr. Zhao Yuyu, etc


Existing monographs or textbooks of the code contribution team in the warehouse: Network Measurement, Measurement and Analysis of Encrypted Traffic, Key Technologies of New Generation Internet Streaming Media Service and Routing, Botnet Detection Technology, Holographic Measurement Method of Network Behavior, etc

School of Cyberspace Science and Engineering, Southeast University, Website: https://cyber.seu.edu.cn

#### Future Expectation

In the future, the network intelligent perception related project and development of the School of Cyberspace Security at Southeast University will be combined with the OpneTSN project, introducing traffic analysis and security perspectives to provide support for the further promotion of OpneTSN!

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request
