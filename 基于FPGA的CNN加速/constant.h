#ifndef _CONSTANT_H_
#define _CONSTANT_H_

#define image_Batch 1
#define INPUT_CHANNEL 2
#define INPUT_SIZE 120
#define CONV_1_INPUT_WH 12
#define CONV_1_INPUT_HT 5
#define CONV_1_INPUT_SIZE 60

#define CONV_1_OUTPUT_WH 10
#define CONV_1_OUTPUT_HT 3
#define CONV_1_OUTPUT_SIZE 30

#define CONV_2_OUTPUT_WH 8
#define CONV_2_OUTPUT_HT 1
#define CONV_2_OUTPUT_SIZE 8

#define CONV_1_TYPE 16
#define CONV_2_TYPE 32

#define FILTER_HT 3
#define FILTER_WH 3
#define FILTER_SIZE 9

#define Sample_Num 100
#define Category 6

#endif
