# 开发平台：
    Vivado HLS 2019.1
# 文件说明：
    constant.h中定义了CNN网络中的常量，如滤波器尺寸
    Conv.h中定义了卷积层函数
    Conv.cpp实现了两个卷积层
    Out_layer.h中定义了全局平均池化(General Average Pooling, GAP)以及输出层
    Out_layer.cpp中实现了全局平均池化和输出层
    main.cpp实现了整个CNN网络
# CNN结构：
    整个CNN结构如下图所示，包含了两个卷积层、一个全局平均池化层和一个输出层
    其中卷积层采用深度可分离卷积，有效地降低了网络的运算量和参数量
![Image test](CNN.jpg)