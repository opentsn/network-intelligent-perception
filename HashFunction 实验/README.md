# Hash 函数

#### 介绍

该开源项目为东南大学网络空间安全学院必修课“网络测量学”课程内必修实验项目。

IP流的测量和分析是网络管理和网络流量工程技术研究和发展的重要依托。但是随着网络主干流量的迅速增长，网络流量的骤增会导致网络测量设备中的路由器资源迅速枯竭，因此一般采用报文抽样技术对原始报文进行过滤。然而即使使用报文抽样后,维护所有的流依旧需要大量的内存空间。因此主要关注的是占用网络带宽超过一定阀值的流.所以采用基于哈希函数的流抽样技术,哈希函数是流抽样技术的核心。

配套教材：《网络测量学》东南大学出版社


#### 实验基本原理

哈希函数算法的原理是将任意可变长度的输入，通过给定的哈希函数变成固定长度的输出。这个映射的规则就是对应的Hash算法，而原始数据映射后的二进制串就是哈希值。随着输入数据的增多，就会难以避免地出现哈希值冲突的现象，即不同的输入值经过哈希函数的计算后得到相同的哈希值。为了尽可能的避免发生冲突，一方面我们可以减少输入的数据量，另一方面可以使用随机性更强的哈希算法以获取随机性高的哈希值。对于高速网络测量而言，减少输入的数据量几乎是无法做到的，于是我们进而希望获得随机性强的哈希函数用于减少冲突。同时，在高速网络测量过程中同时还需要兼顾哈希算法的运行效率，保证算法的运行时间处于可接受的范围之内。

基于以上原因，本仓库中设置了一个实验用于比较不同哈希函数的性能，主要通过对哈希函数的输出哈希值进行分析进而分析各种哈希函数之间的性能差异。


#### 软件架构


本仓库中包含的各类方法，可直接使用相应语言对应的IDE编译器即可运行

- HashFunction.h 中整理了数种常用的哈希函数
- Function.h 和Measurement.h 中实现了调用原始数据进行哈希运算得到哈希值，并根据得到的哈希值给出了评价哈希函数的性能指标。
- Main.c 中调用了上述的函数并输出比较结果




#### 使用说明
相应实验的复现，可参考相关论文或者作者上传的实验报告
