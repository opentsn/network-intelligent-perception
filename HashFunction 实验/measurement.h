#ifndef _MEASUREMENT_H_
#define _MEASUREMENT_H_

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


int BitCount16(unsigned short n)//count 1bit in 16bit num
{
    unsigned int c =0 ;
    for (c =0; n; n >>=1) 
        c += n &1 ; 
    return c ;
}

int BitCount32(unsigned int n)//count 1bit in 32bit num
{
    unsigned int c =0 ;
    for (c =0; n; n >>=1) 
        c += n &1 ; 
    return c ;
}

float BitEntropy16(unsigned short a)//16bit entropy
{
    double i,k,n;
    unsigned short x=a;
    i=BitCount16(x);
    k=i/16;
    n=(-(k*log2(k)+(1-k)*log2(1-k)));
    return n;
}

float BitEntropy32(unsigned int a)//32bit entropy
{
    double i,k,n;
    unsigned int y=a;
    i=BitCount32(y);
    k=i/32;
    n=(-(k*log2(k)+(1-k)*log2(1-k)));
    return n;
}


#endif