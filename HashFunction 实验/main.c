#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include "HashFunction.h"
#include "measurement.h"
#include "function.h"



int main()
{
	clock_t begin, end;
	double cost;
    begin = clock();
	Initialize();
	Average32BitEntropy();  //计算所有返回值类型为32比特位数的位熵，并取平均值
	Average16BitEntropy();  //计算所有返回值类型为16比特位数的位熵，并取平均值
	ConflictRate32(); //计算冲突率
	ConflictRate16();
	end = clock();
    cost = (double)(end - begin)/CLOCKS_PER_SEC;//计算程序运行时间
    printf(" time cost is: %lf secs\n",  cost);
}





