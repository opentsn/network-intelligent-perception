import csv
import os


# # 输入文件A的名称
# file_a_name = input("请输入文件A的名称：")


# # 输入文件B的名称
# file_b_name = input("请输入文件B的名称：")

# # 输入新文件C的名称
# file_c_name = input("请输入新文件C的名称：")

# # 输入新文件C的表头
# #new_header = input("请输入新文件C的表头（以逗号分隔）：")


def merge1(file_a_name, file_b_name):
    # 读取文件A的最后一列
    with open(os.path.join(file_a_name), 'r', newline='') as file_a:
        reader_a = csv.reader(file_a)
        last_col_a = [row[-1] for row in reader_a][1::]
        
    # 读取文件B的最后一列
    with open(os.path.join(file_b_name), 'r', newline='') as file_b:
        reader_b = csv.reader(file_b)
        last_col_b = [row[-1] for row in reader_b][1::]
    
    file_a.close()
    file_b.close()

    return last_col_a, last_col_b

# import result_report as rr
# from pip._vendor.distlib.compat import raw_input
# import csv
# import numpy as np
# import matplotlib.pyplot as plt

# y_true, y_pred = merge1('25test.csv', 'Chenghuang.csv')


# # rr.report(y_true, y_pred, ['1', '2', '3','4','5','6'])
# rr.report(y_true, y_pred, ['No Tethering', 'Tethering'])

