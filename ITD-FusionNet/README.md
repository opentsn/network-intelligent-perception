# ITD-FusionNet for Detecting Portable-Hotspot Behavioral Devices

#### Introduction
The behavior of Portable Hotspots (PHs), where mobile devices share internet connectivity using their own communication media, not only exerts pressure on mobile internet operations and affects revenue distribution but also introduces potential risks to network security by obscuring internal network structures. With the widespread adoption of encrypted traffic, a significant increase in user awareness towards privacy protection, and the gradual inefficacy of traditional detection methods in the context of emerging technologies, particularly with the rise of novel cyberspace such as 5G converged networks, effectively identifying and managing PHed-behavior (also known as tethering) has become a pressing technical challenge. Addressing this situation, our study deeply analyzes the performance characteristics of PHed-devices in data transmission and processing and traffic forwarding and innovatively proposes the "Intranet Tethered Delay" (ITD) measure. Based on ITD, we design and implement a detection model that integrates ensemble learning and deep learning techniques named ITD-FusionNet. Extensive experimental validation demonstrates that ITD-FusionNet performs exceptionally in identifying PHed-devices, achieving an F1 score of 99\% in a closed testing environment and demonstrating good generalization capabilities in real-world scenarios. Compared to the traffic statistical feature methods employed in existing research or the state-of-the-art, our approach improves accuracy by over 20 percentage points. Furthermore, the model maintains high recognition accuracy when confronted with various evasion strategies, validating its robustness and effectiveness in complex network environments.

#### Architecture

![Architecture of PHed-behavior devices detection based on ITD and ITD FusionNet.](image.png)

#### Previous work

DAI X L, CHENG G, LU G Y, et al. Tethering behavior detection architecture based on RTT measurement of TCP flows[J]. Journal of Beijing University of Aeronautics and Astronautics, 2023, 49(6): 1414-1423 (in Chinese).