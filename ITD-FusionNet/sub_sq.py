import numpy as np  
import copy

  
def get_subsequence_between_percentiles(arr, k, j):  
    # import numpy as np
    # 确保k < j  
    if k >= j:  
        raise ValueError("k must be less than j")  
      
    # 将列表转换为numpy数组  
    arr = np.array(arr) 

    # a = round(np.percentile(arr, k), 4) 
    a = np.percentile(arr, k, interpolation='lower')
    b = np.percentile(arr, j, interpolation='higher')


    indices = np.where(arr < a)  # 找到小于等于3的元素的索引  
    arr_new = np.delete(arr, indices[0])  # 删除这些索引处的元素（跳过第一个索引0，因为我们通常不想删除它）  
    indices1 = np.where(arr_new > b) 
    arr_new1 = np.delete(arr_new, indices1[0])#[indices1[0] > 0])  # 删除这些索引处的元素（跳过第一个索引0，因为我们通常不想删除它
    
    # 提取子序列  
    subsequence = arr_new1.tolist()  # 包括idx_j对应的元素  
    return subsequence


def min_max_scaler(data, feature_range=(0, 1), epsilon=1e-5):  
    if len(data) > 1:
        data_min, data_max = np.min(data), np.max(data)  
        scale = (feature_range[1] - feature_range[0]) / max(data_max - data_min, epsilon)  
        return (feature_range[0] + (data - data_min) * scale + epsilon).tolist()
    else:
        return [data[0]] * 5

def bipolar_scaling(data, epsilon=1e-5):  
    min_val = np.min(data)  
    max_val = np.max(data)  
    normalized_data = 2 * ((data - min_val) / max((max_val - min_val), epsilon)) - 1 + epsilon
    return normalized_data.tolist()

def get_k_sublist(data, k):
    if k == 0:
        raise ValueError("k must be greater than 0!")
    data01 = copy.deepcopy(data)
    if len(data) > 2:
        data01_sort = sorted(data01)
        m = int(len(data01)*k/100)
        if m > 1:
            data02_sort = data01_sort[0: (m + 1)]
        else:
            data02_sort = data01_sort[0: 2]
    else:
        data02_sort = data01
    return data02_sort


def zscore_nor(data):  
    if len(data) > 1:
        # 计算均值  
        mean = np.mean(data)  
        
        # 计算标准差，使用ddof=1以得到样本标准差的无偏估计  
        std_dev = np.std(data, ddof=1)  
        
        # 检查标准差是否为0（即所有元素相等）  
        if std_dev == 0:  
            # 如果标准差为0，则直接返回0的数组，因为Z-score将为 (x - mean) / 0  
            if data[0] == 0.000011:
                AA = (np.zeros_like(data, dtype=float) + 0.000011).tolist()
            else:
                AA = (np.zeros_like(data, dtype=float) + 0.00001).tolist()
                
            return AA
        else:  
            # 否则，进行Z-score归一化  
            return ((data - mean) / std_dev).tolist()
    else:
        return [data[0]] * 5
        # return [0.0000121] * 5



# a = [1,2,3,4,5,6,7,11]
# dd = a - np.min(a)
# dd = np.add(dd, 13e-6)
# # dd = dd + 13e-6
# de = dd.tolist()
# print(dd)
# print(de)
# dc = np.add((a - np.min(a)), (13e(1-7))).tolist()
# print(dc)
# exit()
# b = []
# for i in a:
#     i -= np.min(a)
#     i += 1e-5
#     b.append(i)
# print(b)


# print(zscore_nor(a))
# print(zscore_nor(b))

# c = 0.000013
# d = 13e-6
# if c == d:
#     print("!")

# a = [1,2,3,4,5,6,7,11]
# print(np.std(a))
# b = zscore_nor(a)
# print(b)
# print(np.std(b))
# c = min_max_scaler(b)
# print(c)
# print(np.std(c))
