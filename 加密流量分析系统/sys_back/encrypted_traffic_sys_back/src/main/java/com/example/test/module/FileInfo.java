package com.example.test.module;

public class FileInfo {
    private String name;
    private String size;
    private String time;
    private Boolean isDir;

    public FileInfo(String name, String size, String time, Boolean isDir) {
        this.name = name;
        this.size = size;
        this.time = time;
        this.isDir = isDir;
    }

    public Boolean getDir() {
        return isDir;
    }

    public void setDir(Boolean dir) {
        isDir = dir;
    }

    public FileInfo() {
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return "FileInfo{" +
                "name='" + name + '\'' +
                ", size='" + size + '\'' +
                ", time='" + time + '\'' +
                ", isDir=" + isDir +
                '}';
    }
}
