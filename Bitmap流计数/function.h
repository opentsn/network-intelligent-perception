//hash函数
#include <stdio.h>
#include <stdlib.h>
#include "Bitmap.h"
#include <math.h>

unsigned int BKDRhash(const char *str){
    unsigned int seed=31;
    unsigned int hash=0;
    while(*str){
        hash=hash*seed+(*str++);
    }
    return (hash & 0x7FFFFFFF);
}

//计算哈希空间
unsigned int Hspace(FILE *f){
    double e;
    unsigned int n=0;
    char row[1024];
    while (fgets(row, 1024, f)!=NULL)
        n++;

    printf("可接受误差：");
    scanf("%lf",&e);
    n=n/log(n*e*e+1);

    return n;
}

//位图计数算法
unsigned int BM_COUNT(BitMap *bm,FILE *f,unsigned int hspace){
    //unsigned int bitnum=MAX_BIT/v;
    char row[1024];
    char *flowid;
    unsigned int t=0;
    unsigned int hash=1;
    while (fgets(row, 1024, f)!=NULL) {//按行读取数据
        flowid=strtok(row, ","); 
        hash=BKDRhash(flowid);
        hash%=hspace;//对整个哈希空间做求余运算
        if(hash!=t&&hash<(bm->size)){
            BMSet1(bm,hash);
            t=hash;
        }
        else{
            continue;
        }
    }
    unsigned int count=Count(bm);

    return count;
}

//估值计算
unsigned int Result(unsigned int count,unsigned int bitnum){
    double i,j;
    unsigned int result;
    i=(bitnum*1.0)/(bitnum-count);
    j=log(i);
    result=bitnum*j;

    return result;
}