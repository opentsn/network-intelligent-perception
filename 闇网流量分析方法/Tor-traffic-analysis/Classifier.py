import pandas as pd
import joblib
import sys
import numpy as np
from prettytable import PrettyTable
import Models_Classifier as models
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
from sklearn.metrics import precision_score
from sklearn import metrics
import time

ALL_features = ['duration', 'srcIP', 'srcPort', 'dstIP', 'dstPort', 'A_timeFirst','A_timeLast', 'A_duration', 'A_numHdrDesc', 'A_numHdrs','A_hdrDesc', 'A_l4Proto', 'A_macStat', 'A_macPairs','A_srcMac_dstMac_numP', 'A_dstPortClassN', 'A_dstPortClass','A_numPktsSnt', 'A_numPktsRcvd', 'A_numBytesSnt', 'A_numBytesRcvd','A_minPktSz', 'A_maxPktSz', 'A_avePktSize', 'A_stdPktSize','A_minIAT', 'A_maxIAT', 'A_aveIAT', 'A_stdIAT', 'A_pktps','A_bytps', 'A_pktAsm', 'A_bytAsm', 'A_tcpFStat', 'A_ipMindIPID','A_ipMaxdIPID', 'A_ipMinTTL', 'A_ipMaxTTL', 'A_ipTTLChg','A_ipTOS', 'A_ipFlags', 'A_ipOptCnt', 'A_ipOptCpCl_Num','A_ip6OptCntHH_D', 'A_ip6OptHH_D', 'A_tcpISeqN', 'A_tcpPSeqCnt','A_tcpSeqSntBytes', 'A_tcpSeqFaultCnt', 'A_tcpPAckCnt','A_tcpFlwLssAckRcvdBytes', 'A_tcpAckFaultCnt', 'A_tcpInitWinSz','A_tcpAveWinSz', 'A_tcpMinWinSz', 'A_tcpMaxWinSz','A_tcpWinSzDwnCnt', 'A_tcpWinSzUpCnt', 'A_tcpWinSzChgDirCnt','A_tcpWinSzThRt', 'A_tcpFlags', 'A_tcpAnomaly', 'A_tcpOptPktCnt','A_tcpOptCnt', 'A_tcpOptions', 'A_tcpMSS', 'A_tcpWS', 'A_tcpMPTBF','A_tcpMPF', 'A_tcpMPAID', 'A_tcpMPdssF', 'A_tcpSSASAATrip','A_tcpRTTAckTripMin', 'A_tcpRTTAckTripMax', 'A_tcpRTTAckTripAve','A_tcpRTTAckTripJitAve', 'A_tcpRTTSseqAA', 'A_tcpRTTAckJitAve','A_icmpStat', 'A_icmpTCcnt', 'A_icmpBFTypH_TypL_Code','A_icmpTmGtw', 'A_icmpEchoSuccRatio', 'A_icmpPFindex', 'A_connSip','A_connDip', 'A_connSipDip', 'A_connSipDprt', 'A_connF','A_nFpCnt', 'A_tCnt', 'A_Ps_Iat_Cnt_PsCnt_IatCnt', 'A_dsMinPl','A_dsMaxPl', 'A_dsMeanPl', 'A_dsLowQuartilePl', 'A_dsMedianPl','A_dsUppQuartilePl', 'A_dsIqdPl', 'A_dsModePl', 'A_dsRangePl','A_dsStdPl', 'A_dsRobStdPl', 'A_dsSkewPl', 'A_dsExcPl','A_dsMinIat', 'A_dsMaxIat', 'A_dsMeanIat', 'A_dsLowQuartileIat','A_dsMedianIat', 'A_dsUppQuartileIat', 'A_dsIqdIat', 'A_dsModeIat','A_dsRangeIat', 'A_dsStdIat', 'A_dsRobStdIat', 'A_dsSkewIat','A_dsExcIat', 'B_timeFirst', 'B_timeLast', 'B_duration','B_numHdrDesc', 'B_numHdrs', 'B_hdrDesc', 'B_l4Proto', 'B_macStat','B_macPairs', 'B_srcMac_dstMac_numP', 'B_dstPortClassN','B_dstPortClass', 'B_numPktsSnt', 'B_numPktsRcvd', 'B_numBytesSnt','B_numBytesRcvd', 'B_minPktSz', 'B_maxPktSz', 'B_avePktSize','B_stdPktSize', 'B_minIAT', 'B_maxIAT', 'B_aveIAT', 'B_stdIAT','B_pktps', 'B_bytps', 'B_pktAsm', 'B_bytAsm', 'B_tcpFStat','B_ipMindIPID', 'B_ipMaxdIPID', 'B_ipMinTTL', 'B_ipMaxTTL','B_ipTTLChg', 'B_ipTOS', 'B_ipFlags', 'B_ipOptCnt','B_ipOptCpCl_Num', 'B_ip6OptCntHH_D', 'B_ip6OptHH_D', 'B_tcpISeqN','B_tcpPSeqCnt', 'B_tcpSeqSntBytes', 'B_tcpSeqFaultCnt','B_tcpPAckCnt', 'B_tcpFlwLssAckRcvdBytes', 'B_tcpAckFaultCnt','B_tcpInitWinSz', 'B_tcpAveWinSz', 'B_tcpMinWinSz','B_tcpMaxWinSz', 'B_tcpWinSzDwnCnt', 'B_tcpWinSzUpCnt','B_tcpWinSzChgDirCnt', 'B_tcpWinSzThRt', 'B_tcpFlags','B_tcpAnomaly', 'B_tcpOptPktCnt', 'B_tcpOptCnt', 'B_tcpOptions','B_tcpMSS', 'B_tcpWS', 'B_tcpMPTBF', 'B_tcpMPF', 'B_tcpMPAID','B_tcpMPdssF', 'B_tcpSSASAATrip', 'B_tcpRTTAckTripMin','B_tcpRTTAckTripMax', 'B_tcpRTTAckTripAve','B_tcpRTTAckTripJitAve', 'B_tcpRTTSseqAA', 'B_tcpRTTAckJitAve','B_icmpStat', 'B_icmpTCcnt', 'B_icmpBFTypH_TypL_Code','B_icmpTmGtw', 'B_icmpEchoSuccRatio', 'B_icmpPFindex', 'B_connSip','B_connDip', 'B_connSipDip', 'B_connSipDprt', 'B_connF','B_nFpCnt', 'B_tCnt', 'B_Ps_Iat_Cnt_PsCnt_IatCnt', 'B_dsMinPl','B_dsMaxPl', 'B_dsMeanPl', 'B_dsLowQuartilePl', 'B_dsMedianPl','B_dsUppQuartilePl', 'B_dsIqdPl', 'B_dsModePl', 'B_dsRangePl','B_dsStdPl', 'B_dsRobStdPl', 'B_dsSkewPl', 'B_dsExcPl','B_dsMinIat', 'B_dsMaxIat', 'B_dsMeanIat', 'B_dsLowQuartileIat','B_dsMedianIat', 'B_dsUppQuartileIat', 'B_dsIqdIat', 'B_dsModeIat','B_dsRangeIat', 'B_dsStdIat', 'B_dsRobStdIat', 'B_dsSkewIat','B_dsExcIat', '0A_PL', '0A_IAT', '1A_PL', '1A_IAT', '2A_PL','2A_IAT', '3A_PL', '3A_IAT', '4A_PL', '4A_IAT', '5A_PL', '5A_IAT','6A_PL', '6A_IAT', '7A_PL', '7A_IAT', '8A_PL', '8A_IAT', '9A_PL','9A_IAT', '10A_PL', '10A_IAT', '11A_PL', '11A_IAT', '12A_PL','12A_IAT', '13A_PL', '13A_IAT', '14A_PL', '14A_IAT', '15A_PL','15A_IAT', '16A_PL', '16A_IAT', '17A_PL', '17A_IAT', '18A_PL','18A_IAT', '19A_PL', '19A_IAT', '0B_PL', '0B_IAT', '1B_PL','1B_IAT', '2B_PL', '2B_IAT', '3B_PL', '3B_IAT', '4B_PL', '4B_IAT','5B_PL', '5B_IAT', '6B_PL', '6B_IAT', '7B_PL', '7B_IAT', '8B_PL','8B_IAT', '9B_PL', '9B_IAT', '10B_PL', '10B_IAT', '11B_PL','11B_IAT', '12B_PL', '12B_IAT', '13B_PL', '13B_IAT', '14B_PL','14B_IAT', '15B_PL', '15B_IAT', '16B_PL', '16B_IAT', '17B_PL','17B_IAT', '18B_PL', '18B_IAT', '19B_PL', '19B_IAT', 'class1','class2', 'class']

feature = ['duration', 'srcIP', 'srcPort', 'dstIP', 'dstPort', 'A_timeFirst', 'A_timeLast', 'A_duration', 'A_numHdrDesc', 'A_numHdrs', 'A_hdrDesc', 'A_l4Proto', 'A_macStat', 'A_macPairs', 'A_srcMac_dstMac_numP', 'A_dstPortClassN', 'A_dstPortClass', 'A_numPktsSnt', 'A_numPktsRcvd', 'A_numBytesSnt', 'A_numBytesRcvd', 'A_minPktSz', 'A_maxPktSz', 'A_avePktSize', 'A_stdPktSize', 'A_minIAT', 'A_maxIAT', 'A_aveIAT', 'A_stdIAT', 'A_pktps', 'A_bytps', 'A_pktAsm', 'A_bytAsm', 'A_tcpFStat', 'A_ipMindIPID', 'A_ipMaxdIPID', 'A_ipMinTTL', 'A_ipMaxTTL', 'A_ipTTLChg', 'A_ipTOS', 'A_ipFlags', 'A_ipOptCnt', 'A_ipOptCpCl_Num', 'A_ip6OptCntHH_D', 'A_ip6OptHH_D', 'A_tcpISeqN', 'A_tcpPSeqCnt', 'A_tcpSeqSntBytes', 'A_tcpSeqFaultCnt', 'A_tcpPAckCnt', 'A_tcpFlwLssAckRcvdBytes', 'A_tcpAckFaultCnt', 'A_tcpInitWinSz', 'A_tcpAveWinSz', 'A_tcpMinWinSz', 'A_tcpMaxWinSz', 'A_tcpWinSzDwnCnt', 'A_tcpWinSzUpCnt', 'A_tcpWinSzChgDirCnt', 'A_tcpWinSzThRt', 'A_tcpFlags', 'A_tcpAnomaly', 'A_tcpOptPktCnt', 'A_tcpOptCnt', 'A_tcpOptions', 'A_tcpMSS', 'A_tcpWS', 'A_tcpMPTBF', 'A_tcpMPF', 'A_tcpMPAID', 'A_tcpMPdssF', 'A_tcpTmS', 'A_tcpTmER', 'A_tcpEcI', 'A_tcpUtm', 'A_tcpBtm', 'A_tcpSSASAATrip', 'A_tcpRTTAckTripMin', 'A_tcpRTTAckTripMax', 'A_tcpRTTAckTripAve', 'A_tcpRTTAckTripJitAve', 'A_tcpRTTSseqAA', 'A_tcpRTTAckJitAve', 'A_icmpStat', 'A_icmpTCcnt', 'A_icmpBFTypH_TypL_Code', 'A_icmpTmGtw', 'A_icmpEchoSuccRatio', 'A_icmpPFindex', 'A_connSip', 'A_connDip', 'A_connSipDip', 'A_connSipDprt', 'A_connF', 'A_nFpCnt', 'A_tCnt', 'A_Ps_Iat_Cnt_PsCnt_IatCnt', 'A_dsMinPl', 'A_dsMaxPl', 'A_dsMeanPl', 'A_dsLowQuartilePl', 'A_dsMedianPl', 'A_dsUppQuartilePl', 'A_dsIqdPl', 'A_dsModePl', 'A_dsRangePl', 'A_dsStdPl', 'A_dsRobStdPl', 'A_dsSkewPl', 'A_dsExcPl', 'A_dsMinIat', 'A_dsMaxIat', 'A_dsMeanIat', 'A_dsLowQuartileIat', 'A_dsMedianIat', 'A_dsUppQuartileIat', 'A_dsIqdIat', 'A_dsModeIat', 'A_dsRangeIat', 'A_dsStdIat', 'A_dsRobStdIat', 'A_dsSkewIat', 'A_dsExcIat', 'B_timeFirst', 'B_timeLast', 'B_duration', 'B_numHdrDesc', 'B_numHdrs', 'B_hdrDesc', 'B_l4Proto', 'B_macStat', 'B_macPairs', 'B_srcMac_dstMac_numP', 'B_dstPortClassN', 'B_dstPortClass', 'B_numPktsSnt', 'B_numPktsRcvd', 'B_numBytesSnt', 'B_numBytesRcvd', 'B_minPktSz', 'B_maxPktSz', 'B_avePktSize', 'B_stdPktSize', 'B_minIAT', 'B_maxIAT', 'B_aveIAT', 'B_stdIAT', 'B_pktps', 'B_bytps', 'B_pktAsm', 'B_bytAsm', 'B_tcpFStat', 'B_ipMindIPID', 'B_ipMaxdIPID', 'B_ipMinTTL', 'B_ipMaxTTL', 'B_ipTTLChg', 'B_ipTOS', 'B_ipFlags', 'B_ipOptCnt', 'B_ipOptCpCl_Num', 'B_ip6OptCntHH_D', 'B_ip6OptHH_D', 'B_tcpISeqN', 'B_tcpPSeqCnt', 'B_tcpSeqSntBytes', 'B_tcpSeqFaultCnt', 'B_tcpPAckCnt', 'B_tcpFlwLssAckRcvdBytes', 'B_tcpAckFaultCnt', 'B_tcpInitWinSz', 'B_tcpAveWinSz', 'B_tcpMinWinSz', 'B_tcpMaxWinSz', 'B_tcpWinSzDwnCnt', 'B_tcpWinSzUpCnt', 'B_tcpWinSzChgDirCnt', 'B_tcpWinSzThRt', 'B_tcpFlags', 'B_tcpAnomaly', 'B_tcpOptPktCnt', 'B_tcpOptCnt', 'B_tcpOptions', 'B_tcpMSS', 'B_tcpWS', 'B_tcpMPTBF', 'B_tcpMPF', 'B_tcpMPAID', 'B_tcpMPdssF', 'B_tcpTmS', 'B_tcpTmER', 'B_tcpEcI', 'B_tcpUtm', 'B_tcpBtm', 'B_tcpSSASAATrip', 'B_tcpRTTAckTripMin', 'B_tcpRTTAckTripMax', 'B_tcpRTTAckTripAve', 'B_tcpRTTAckTripJitAve', 'B_tcpRTTSseqAA', 'B_tcpRTTAckJitAve', 'B_icmpStat', 'B_icmpTCcnt', 'B_icmpBFTypH_TypL_Code', 'B_icmpTmGtw', 'B_icmpEchoSuccRatio', 'B_icmpPFindex', 'B_connSip', 'B_connDip', 'B_connSipDip', 'B_connSipDprt', 'B_connF', 'B_nFpCnt', 'B_tCnt', 'B_Ps_Iat_Cnt_PsCnt_IatCnt', 'B_dsMinPl', 'B_dsMaxPl', 'B_dsMeanPl', 'B_dsLowQuartilePl', 'B_dsMedianPl', 'B_dsUppQuartilePl', 'B_dsIqdPl', 'B_dsModePl', 'B_dsRangePl', 'B_dsStdPl', 'B_dsRobStdPl', 'B_dsSkewPl', 'B_dsExcPl', 'B_dsMinIat', 'B_dsMaxIat', 'B_dsMeanIat', 'B_dsLowQuartileIat', 'B_dsMedianIat', 'B_dsUppQuartileIat', 'B_dsIqdIat', 'B_dsModeIat', 'B_dsRangeIat', 'B_dsStdIat', 'B_dsRobStdIat', 'B_dsSkewIat', 'B_dsExcIat', '0A_PL', '0A_IAT', '1A_PL', '1A_IAT', '2A_PL', '2A_IAT', '3A_PL', '3A_IAT', '4A_PL', '4A_IAT', '5A_PL', '5A_IAT', '6A_PL', '6A_IAT', '7A_PL', '7A_IAT', '8A_PL', '8A_IAT', '9A_PL', '9A_IAT', '10A_PL', '10A_IAT', '11A_PL', '11A_IAT', '12A_PL', '12A_IAT', '13A_PL', '13A_IAT', '14A_PL', '14A_IAT', '15A_PL', '15A_IAT', '16A_PL', '16A_IAT', '17A_PL', '17A_IAT', '18A_PL', '18A_IAT', '19A_PL', '19A_IAT', '0B_PL', '0B_IAT', '1B_PL', '1B_IAT', '2B_PL', '2B_IAT', '3B_PL', '3B_IAT', '4B_PL', '4B_IAT', '5B_PL', '5B_IAT', '6B_PL', '6B_IAT', '7B_PL', '7B_IAT', '8B_PL', '8B_IAT', '9B_PL', '9B_IAT', '10B_PL', '10B_IAT', '11B_PL', '11B_IAT', '12B_PL', '12B_IAT', '13B_PL', '13B_IAT', '14B_PL', '14B_IAT', '15B_PL', '15B_IAT', '16B_PL', '16B_IAT', '17B_PL', '17B_IAT', '18B_PL', '18B_IAT', '19B_PL', '19B_IAT', 'class1', 'class2', 'class']

numfeature = ['A_numPktsSnt', 'A_numPktsRcvd', 'A_numBytesSnt', 'A_numBytesRcvd', 'A_minPktSz', 'A_maxPktSz', 'A_avePktSize', 'A_stdPktSize', 'A_minIAT', 'A_maxIAT', 'A_aveIAT', 'A_stdIAT', 'A_pktps', 'A_bytps', 'A_pktAsm', 'A_bytAsm',  'A_ipOptCnt', 'A_tcpISeqN', 'A_tcpPSeqCnt', 'A_tcpSeqSntBytes', 'A_tcpSeqFaultCnt', 'A_tcpPAckCnt', 'A_tcpFlwLssAckRcvdBytes', 'A_tcpAckFaultCnt', 'A_tcpInitWinSz', 'A_tcpAveWinSz', 'A_tcpMinWinSz', 'A_tcpMaxWinSz', 'A_tcpWinSzDwnCnt', 'A_tcpWinSzUpCnt', 'A_tcpWinSzChgDirCnt', 'A_tcpWinSzThRt', 'A_tcpOptPktCnt', 'A_tcpOptCnt', 'A_tcpMSS', 'A_tcpWS', 'A_tcpSSASAATrip', 'A_tcpRTTAckTripMin', 'A_tcpRTTAckTripMax', 'A_tcpRTTAckTripAve', 'A_tcpRTTAckTripJitAve', 'A_tcpRTTSseqAA', 'A_tcpRTTAckJitAve',  'A_connF', 'A_nFpCnt', 'A_tCnt', 'A_dsMinPl', 'A_dsMaxPl', 'A_dsMeanPl', 'A_dsLowQuartilePl', 'A_dsMedianPl', 'A_dsUppQuartilePl', 'A_dsIqdPl', 'A_dsModePl', 'A_dsRangePl', 'A_dsStdPl', 'A_dsRobStdPl', 'A_dsSkewPl', 'A_dsExcPl', 'A_dsMinIat', 'A_dsMaxIat', 'A_dsMeanIat', 'A_dsLowQuartileIat', 'A_dsMedianIat', 'A_dsUppQuartileIat', 'A_dsIqdIat', 'A_dsModeIat', 'A_dsRangeIat', 'A_dsStdIat', 'A_dsRobStdIat', 'A_dsSkewIat', 'A_dsExcIat','B_numPktsSnt', 'B_numPktsRcvd', 'B_numBytesSnt', 'B_numBytesRcvd', 'B_minPktSz', 'B_maxPktSz', 'B_avePktSize', 'B_stdPktSize', 'B_minIAT', 'B_maxIAT', 'B_aveIAT', 'B_stdIAT', 'B_pktps', 'B_bytps', 'B_pktAsm', 'B_bytAsm', 'B_ipOptCnt', 'B_tcpISeqN', 'B_tcpPSeqCnt', 'B_tcpSeqSntBytes', 'B_tcpSeqFaultCnt', 'B_tcpPAckCnt', 'B_tcpFlwLssAckRcvdBytes', 'B_tcpAckFaultCnt', 'B_tcpInitWinSz', 'B_tcpAveWinSz', 'B_tcpMinWinSz', 'B_tcpMaxWinSz', 'B_tcpWinSzDwnCnt', 'B_tcpWinSzUpCnt', 'B_tcpWinSzChgDirCnt', 'B_tcpWinSzThRt', 'B_tcpOptPktCnt', 'B_tcpOptCnt', 'B_tcpMSS', 'B_tcpWS', 'B_tcpSSASAATrip', 'B_tcpRTTAckTripMin', 'B_tcpRTTAckTripMax', 'B_tcpRTTAckTripAve', 'B_tcpRTTAckTripJitAve', 'B_tcpRTTSseqAA', 'B_tcpRTTAckJitAve', 'B_connF', 'B_nFpCnt', 'B_tCnt', 'B_dsMinPl', 'B_dsMaxPl', 'B_dsMeanPl', 'B_dsLowQuartilePl', 'B_dsMedianPl', 'B_dsUppQuartilePl', 'B_dsIqdPl', 'B_dsModePl', 'B_dsRangePl', 'B_dsStdPl', 'B_dsRobStdPl', 'B_dsSkewPl', 'B_dsExcPl', 'B_dsMinIat', 'B_dsMaxIat', 'B_dsMeanIat', 'B_dsLowQuartileIat', 'B_dsMedianIat', 'B_dsUppQuartileIat', 'B_dsIqdIat', 'B_dsModeIat', 'B_dsRangeIat', 'B_dsStdIat', 'B_dsRobStdIat', 'B_dsSkewIat', 'B_dsExcIat', 'class']

pl_iat = ['0A_PL', '0A_IAT', '1A_PL', '1A_IAT', '2A_PL', '2A_IAT', '3A_PL', '3A_IAT', '4A_PL', '4A_IAT', '5A_PL', '5A_IAT', '6A_PL', '6A_IAT', '7A_PL', '7A_IAT', '8A_PL', '8A_IAT', '9A_PL', '9A_IAT', '10A_PL', '10A_IAT', '11A_PL', '11A_IAT', '12A_PL', '12A_IAT', '13A_PL', '13A_IAT', '14A_PL', '14A_IAT', '15A_PL', '15A_IAT', '16A_PL', '16A_IAT', '17A_PL', '17A_IAT', '18A_PL', '18A_IAT', '19A_PL', '19A_IAT', '0B_PL', '0B_IAT', '1B_PL', '1B_IAT', '2B_PL', '2B_IAT', '3B_PL', '3B_IAT', '4B_PL', '4B_IAT', '5B_PL', '5B_IAT', '6B_PL', '6B_IAT', '7B_PL', '7B_IAT', '8B_PL', '8B_IAT', '9B_PL', '9B_IAT', '10B_PL', '10B_IAT', '11B_PL', '11B_IAT', '12B_PL', '12B_IAT', '13B_PL', '13B_IAT', '14B_PL', '14B_IAT', '15B_PL', '15B_IAT', '16B_PL', '16B_IAT', '17B_PL', '17B_IAT', '18B_PL', '18B_IAT', '19B_PL', '19B_IAT']

# 读取数据
filename = r'J:\博士研究\论文写作\202108第一篇小论文\dataset\2.预处理后数据\pre_train.csv'
org_data = pd.read_csv(filename ,low_memory=False, delimiter=',')
data = org_data.replace(' ', 0)

# 选择特征和标签
#data = data[[*pl_iat,'class1']]
#data.loc[data['class1']!='normal','class1']='tor'

select_features = ['B_tcpMSS', 'B_tcpBtm', 'A_tcpBtm', 'A_tcpTmS', 'duration', 'A_tcpTmER', 'B_duration', 'B_tcpTmER', 'A_tcpInitWinSz', 'B_tcpTmS', 'A_tcpMaxWinSz', 'A_tcpAveWinSz', 'A_duration', 'B_numBytesRcvd', 'A_tcpOptCnt', 'A_tcpRTTSseqAA', 'A_dsMaxPl', 'A_numPktsRcvd', 'A_dsRangePl', 'A_tcpOptPktCnt', 'A_dsSkewPl', 'A_tcpSeqSntBytes', 'A_stdPktSize', 'A_tcpMinWinSz', 'B_tcpAveWinSz', 'B_tcpPSeqCnt', 'B_tcpInitWinSz', 'A_numBytesSnt', 'A_bytAsm', 'B_tcpFlwLssAckRcvdBytes']
data = data[[*select_features, 'class1']]

# 得到训练集
dataset = data.values
features = dataset[::, 0:-1]
label = dataset[::, -1]

x_train,x_test,y_train,y_test = train_test_split(features,label,test_size=0.1)

# 选择模型
modeldict = {'c45':models.c45, 'cart':models.cart,'knn':models.knn,'lrc':models.lrc,'rf10':models.rf10,'rf20':models.rf20,'rf30':models.rf30,'gbdt':models.gbdt,'AdaBoost':models.AdaBoost,'gnb':models.gnb,'lda':models.lda,'qda':models.qda,'svm':models.svm}

modeldict = {'c45':models.c45,'knn':models.knn,'rf30':models.rf30,'gbdt':models.gbdt}


## 遍历模型

tabel = PrettyTable(['num','model','accuracy','precision','act\\predict','predict tor','predict nor'])
n = 1
for model in modeldict:
    clf = modeldict[model]()
    start = time.time()
    #clf.fit(x_train,y_train)
    clf.fit(features, label)
    # 保存模型
    joblib.dump(clf, 'models/2_'+model+'.m')


    predict = clf.predict(x_test)
    end = time.time()
    
    #pd_predict = pd.DataFrame({'predict':predict})
    #x_test.index = np.arange(len(x_test))
    #pd_predict.index = np.arange(len(pd_predict))
    #data = pd.concat([x_test,pd_predict], axis=1)
    
    result = accuracy_score(y_test, predict)
    precision = precision_score(y_test, predict, average='macro')
    
    print('------------------------------------------------------------------------------------------------')
    print(model, end-start)
    print(precision)
    confusion_matrix_result = metrics.confusion_matrix(y_test, predict, labels=['audio','mail','p2p','vedio','browser','voip','normal'])
    print(confusion_matrix_result)
    print('------------------------------------------------------------------------------------------------')


    '''
    a = 0
    for i in range(len(precision)):
        a = a+precision[i]
    precision = a/(len(precision))
    '''
    '''
    confusion_matrix_result = metrics.confusion_matrix(y_test, predict)
    tabel.add_row([n,model,result,precision,'tor',confusion_matrix_result[0][0],confusion_matrix_result[0][1]])
    tabel.add_row(['','','','','nor',confusion_matrix_result[1][0],confusion_matrix_result[1][1]])
    '''

    n = n+1

print(tabel)


# 特征排序
'''
clf = models.rf30()
clf.fit(x_train,y_train)
importamce = clf.feature_importances_ / np.max(clf.feature_importances_)
print(sorted(zip(map(lambda x: round(x, 4), importamce), numfeature.pop()), reverse=True))

indices = np.argsort(importamce)[::-1]
a = []
for f in range(x_train.shape[1]):
    a.append(numfeature[indices[f]])
    print("%2d) %-*s %f" % (f + 1, 30, numfeature[indices[f]], importamce[indices[f]]))

print(a)
'''