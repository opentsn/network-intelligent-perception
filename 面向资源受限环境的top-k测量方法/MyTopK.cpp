#include <iostream>
#include <string>
#include <fstream>
#include <set>
#include <map>
#include <algorithm>

#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>

#include "params.h"
#include "BOBHash64.h"

using namespace std;

#ifdef INPUT_PARAMS
string DATASET_PATH;
int TOP_K;
int PACKET_NUM;
int FLOW_NUM;
int COUNTER_NUM;
int DEPTH;
int WITCH;
int L;
int SAMPLING_PROBABILITY_1;
int SAMPLING_PROBABILITY_2;
int SAMPLING_PROBABILITY_3;
#endif

int PACKET_NUM1;
int p_num_caida2018 = 27120000;
int p_num_cernet30 = 17590000;
int p_num_cernet60 = 32670000;

struct _Node {
    string      key;
    byte        cnt;
    _Node(string key = "", byte cnt = 0) : key(key), cnt(cnt) {}
};


class MyHeap {

private:
    int             K;              // 堆的最大元素个数
    _Node*          heap;           // 堆
    int             size_;          // 堆中元素个数
    set<string>     set;            // 空间换时间，减少search的时间
    _Node*          result;


private:
    void down(int u) {
        int t = u;                                                                      // t记录最小值
        if (2 * u <= size_ && heap[2 * u].cnt < heap[t].cnt) t = 2 * u;                 // 左儿子存在，且值比父亲小
        if (2 * u + 1 <= size_ && heap[2 * u + 1].cnt < heap[t].cnt) t = 2 * u + 1;     // 右儿子存在，且值比父亲小
        if (t != u) {
            swap(heap[t], heap[u]);
            down(t);
        }
        return;
    }

    void up(int u) {
        if (u / 2 > 0 && heap[u / 2].cnt > heap[u].cnt) {
            swap(heap[u / 2], heap[u]);
            up(u / 2);
        }
        return;
    }


public:

    MyHeap(int K) {
        this->K = K;
        heap = new _Node[K + 5];
        result = new _Node[K + 5];
    }


    void insert(_Node value) {
        heap[++size_] = value;
        up(size_);
        set.insert(value.key);
    }


    int size() {
        return size_;
    }


    _Node min() {
        return heap[1];
    }

    
    void replace(int idx, _Node value) {
        set.erase(heap[idx].key);
        heap[idx] = value;
        down(idx);
        up(idx);
        set.insert(value.key);
    }


    void change_cnt(int idx, int cnt) {
        heap[idx].cnt = cnt;
        down(idx);
        up(idx);
    }


    int search(string const x) {
        // 空间换时间，减少search的时间
        // set的find的时间复杂度为logn，而遍历heap查找的时间复杂度为n
        // 如果x不存在于堆中（大多数情况下不是topk，因而是不存在的），此时可以将时间复杂度由n降低为logn
        if (set.find(x) == set.end()) {
            return -1;
        }

        for (int i = 1; i <= size_; i++) {
            if (heap[i].key == x) {
                return i;
            }
        }

        throw("???");
        //return -1;
    }


    void sort() {
        int n = size_;
        for (int i = 1; i <= n; i++) {
            result[n - i + 1] = heap[1];
            heap[1] = heap[size_];
            size_--;
            down(1);
        }
    }


    _Node get(int idx) {
        return result[idx];
    }
};


class MyTopK {

private:
    int         K;
    int         depth;
    int         witch;

    byte**      cnts;
    MyHeap*     heap;
    BOBHash64*  bobhash;
    

public:

    MyTopK(int K, int depth, int witch) : K(K), depth(depth), witch(witch) {
        heap = new MyHeap(K);
        bobhash = new BOBHash64(1005);

        cnts = new byte* [depth];
        for (int i = 0; i < depth; i++)
        {
            cnts[i] = new byte[witch];
            for (int j = 0; j < witch; j++)
            {
                cnts[i][j] = 0;
            }
        }
    }


    unsigned long long Hash(string ST) {
        return (bobhash->run(ST.c_str(), ST.size()));
    }


    // 以1/n的概率抽样, n = SAMPLING_PROBABILITY
    bool Sampling(byte cnt) {
        if (cnt <= COUNTER_THRESHOLDS_1) {
            if (rand() % SAMPLING_PROBABILITY_1 == 0) {
                return true;
            }
        }
        else if (cnt <= COUNTER_THRESHOLDS_2) {
            if (rand() % SAMPLING_PROBABILITY_2 == 0) {
                return true;
            }
        }
        else if (cnt <= COUNTER_THRESHOLDS_3) {
            if (rand() % SAMPLING_PROBABILITY_3 == 0) {
                return true;
            }
        }
        return false;
    }


    int Estimation(int cnt) {
        int value = 0;
        if (cnt >= COUNTER_THRESHOLDS_2 + 1) {
            value += (cnt - (COUNTER_THRESHOLDS_2 + 1)) * SAMPLING_PROBABILITY_3;
            cnt = COUNTER_THRESHOLDS_2 + 1;
        }
        if (cnt >= COUNTER_THRESHOLDS_1 + 1) {
            value += (cnt - (COUNTER_THRESHOLDS_1 + 1)) * SAMPLING_PROBABILITY_2;
            cnt = COUNTER_THRESHOLDS_1 + 1;
        }
        if (cnt >= 0) {
            value += cnt * SAMPLING_PROBABILITY_1;
            cnt = 0;
        }
        return value;
    }


    void Insert(string x) {

        unsigned long long H = Hash(x);
        int min = 0xfffffff;

        for (int j = 0; j < depth; j++)
        {
            int Hsh = H % (witch - (2 * depth) + 2 * j + 3);
            
            // 概率抽样
            if (Sampling(cnts[j][Hsh])) {
                if (cnts[j][Hsh] < 0xff) {
                    cnts[j][Hsh] ++;
                }
            }

            if (cnts[j][Hsh] < min) {
                min = cnts[j][Hsh];
            }
        }

        int idx = heap->search(x);

        // 此流之前未被认定为TopK
        if (idx == -1) {
            _Node node(x, min);
            if (heap->size() < K) {
                heap->insert(node);
            }
            else if (min > heap->min().cnt) {
                heap->replace(1, node);
            }
        }

        // 此流之前被认定为TopK
        else {
            heap->change_cnt(idx, min);
        }
    }


    void Work() {
        heap->sort();
    }


    _Node Query(int idx) {
        return heap->get(idx+1);
    }

};
//
//
//string get_port(byte* sz) {
//    char port[10] = { 0 };
//    int a = sz[1];
//    int b = sz[0];
//    sprintf_s(port, "%u", (sz[1] << 8) + sz[0]);
//    return port;
//}


void str2info(string x) {
    byte info[13] = { 0 };
    memcpy(info, x.c_str(), 13);

    char src_ip[20] = { 0 };
    sprintf_s(src_ip, "%u.%u.%u.%u", info[3], info[2], info[1], info[0]);

    char dst_ip[20] = { 0 };
    sprintf_s(dst_ip, "%u.%u.%u.%u", info[7], info[6], info[5], info[4]);

    char src_port[10] = { 0 };
    sprintf_s(src_port, "%u", (info[9] << 8) + info[8]);

    char dst_port[10] = { 0 };
    sprintf_s(dst_port, "%u", (info[11] << 8) + info[10]);

    printf("%-16s| %-16s| %-8s| %-8s| %-6u| ", src_ip, dst_ip, src_port, dst_port, info[12]);
}





// 读取数据集
char a[32];

string Read(ifstream& fin)
{
    fin.read(a, 13);
    /**

    if (!fin) {
        printf("无法读取数据集: %s\n", DATASET_PATH);
        system("pause");
        exit(-1);
    }**/
    a[13] = '\0';
    return a;
}

/*
string Read()
{
    fin.read(a,13);
    a[13]='\0';
    string tmp=a;
    return tmp;
}*/

// 用于统计流的真实数据包数。B是所有的流，C是TopK的流。
map<string, int> B, C;
struct node { string x; int y; };
int cmp(node i, node j) { return i.y > j.y; }


int main()
{

#ifdef INPUT_PARAMS
    cout << "DATASET_PATH：";
    getline(cin, DATASET_PATH);


    cout << "TOP_K：";
    cin >> TOP_K;

    cout << "PACKET_NUM(×1W)：";
    cin >> PACKET_NUM1;
    PACKET_NUM = PACKET_NUM1 * 10000;
    //PACKET_NUM = p_num_caida2018;
    FLOW_NUM = PACKET_NUM;
    int Mem;
    cout << "MEM(KB) = ";
    cin >> Mem;
    Mem = Mem*1024;
    //COUNTER_NUM = 131072 - TOP_K * 13 * 2;
    COUNTER_NUM = Mem - TOP_K * 13 * 2;
    //COUNTER_NUM = 65536;

    cout << "L：";
    cin >> L;
    SAMPLING_PROBABILITY_1 = (L - ((COUNTER_THRESHOLDS_1)-1));
    SAMPLING_PROBABILITY_2 = (SAMPLING_PROBABILITY_1 + 64);
    SAMPLING_PROBABILITY_3 = (SAMPLING_PROBABILITY_2 + 128);

    //cout << "SAMPLING_PROBABILITY_1 = " << SAMPLING_PROBABILITY_1 << endl;
    //cout << "SAMPLING_PROBABILITY_2 = " << SAMPLING_PROBABILITY_2 << endl;
    //cout << "SAMPLING_PROBABILITY_3 = " << SAMPLING_PROBABILITY_3 << endl;

    //cout << "DEPTH：";
    //cin >> DEPTH;
    DEPTH = 3;
    WITCH = COUNTER_NUM / DEPTH;
    cout << "WITCH = " << WITCH << endl << endl;



#endif
    //cout << typeid(DATASET_PATH).name()<<endl;

    if (COUNTER_NUM <= 0) {
        cout << "TOP_K过大，导致COUNTER_NUM为负！\n";
        system("pause");
        exit(-1);
    }

    MyTopK obj(TOP_K, DEPTH, WITCH);

    // 插入
    cout << "插入中......" << endl;
    //ifstream fin(DATASET_PATH, ios::in | ios::binary);
    //string AA = "M:\\2022实验\\Data\\CAIDA2018.dat";
    //string BB = "M:\\2022实验\\Data\\CERNET30";
    //string CC = "M:\\2022实验\\Data\\CERNET60";
    ifstream fin(DATASET_PATH, ios::in | ios::binary);

    for (int i = 1; i <= PACKET_NUM; i++)
    {
        if (i % (PACKET_NUM / 10) == 0) {
            printf("已插入 %d 条数据包信息\n", i);
        }
        //string x = Read(fin);
        string x = Read(fin);
        B[x]++;
        obj.Insert(x);
    }

    obj.Work();

    // 统计流的真实数据包数
    printf("\n正在统计流的真实数据包数......\n");
    node* p = new node[FLOW_NUM + 5];
    int cnt = 0;
    for (map<string, int>::iterator sit = B.begin(); sit != B.end(); sit++)
    {
        if (cnt + 1 > FLOW_NUM) {
            cout << "预设的FLOW_NUM过小，数组溢出" << endl;
            system("pause");
            exit(-1);
        }
        p[++cnt].x = sit->first;
        p[cnt].y = sit->second;
    }
    sort(p + 1, p + cnt + 1, cmp);
    for (int i = 1; i <= TOP_K; i++) {
        C[p[i].x] = p[i].y;
    }
    printf("共有%d条流\n\n", B.size());

    // 计算 PRE, ARE, AAE
    printf("计算实验结果中......\n");
    int sum = 0, AAE = 0; 
    double ARE = 0;
    int max_cnt = -1, est_max_cnt = -1, true_max_cnt = -1;
    for (int i = 0; i < TOP_K; i++)
    {
        _Node node = obj.Query(i);
        string str = node.key;
        int est_num = obj.Estimation(node.cnt);
        int true_num = B[str];
        AAE += abs(true_num - est_num);
        ARE += abs(true_num - est_num) / (true_num + 0.0);
        if (C[str]) sum++;

        if (est_max_cnt < est_num) {
            max_cnt = node.cnt;
            est_max_cnt = est_num;
            true_max_cnt = true_num;
        }
    }



    //printf("%-16s| %-16s| %-8s| %-8s| %-6s| %-8s| %-8s\n", "源IP", "目的IP", "源端口", "目的端口", "协议号", "预估值", "真实值");

    for (int i = 0; i < TOP_K; i++) {
        _Node node = obj.Query(i);
        string str = node.key;
        int est_num = obj.Estimation(node.cnt);
        int true_num = B[str];
        //str2info(str);
        //printf("%-8d| %-8d\n", est_num, true_num);
    }
    cout << "L："<<L<<endl;
    printf("实验结果:\nAccepted: %d/%d  %.10f\nARE: %.10f\nAAE: %.10f\n\n", sum, TOP_K, (sum / (TOP_K + 0.0)), ARE / TOP_K, AAE / (TOP_K + 0.0));
    system("pause");
    //printf("L = %d时，本模型理论上计数器预估上限为%d。\n", L, obj.Estimation(255));
    //printf("本次实验计数器最大的数值为%d，对应预估数值为%d，真实数值为%d\n\n", max_cnt, est_max_cnt, true_max_cnt);

    system("pause");
}