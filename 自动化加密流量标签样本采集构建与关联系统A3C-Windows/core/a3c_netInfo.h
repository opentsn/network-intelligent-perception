#pragma once
#include <winsock2.h>
#include <ws2tcpip.h>
#include <Iphlpapi.h>
#include <TlHelp32.h>
#include <tchar.h>
#include <stdio.h>
#include <vector>
#include <string>



#define UNKNOWN_PID -2
#define UNKNOWN_ERROR -1

#pragma comment(lib, "iphlpapi.lib")
#pragma comment(lib, "ws2_32.lib")
#define TYPE_TCP 6
#define TYPE_UDP 17
#define TYPE_DNS 53
#define TYPE_IP 8
#define TYPE_IP6 56710
//虽然实际应该是0x0800和0x86DD标识IPv4和IPv6
//但是性能优先，就不做端序转化了

typedef struct _EthHeader
{
	byte dst_mac[6];
	byte src_mac[6];
	u_short type;
}EthHeader;

typedef struct _IPHeader
{
	byte ver_hdlen;
	byte service;
	u_short total_length;
	u_short id;
	union {
		u_short flags;
		u_short offset;
	};
	byte ttl;
	byte protocol;
	u_short sum;
	unsigned int src_ip;
	unsigned int dst_ip;
}IPHeader;

typedef struct _IPv6Header
{
	unsigned int ver_tc_flow;
	u_short payload_length;
	byte next_header;
	byte hop_limit;
	byte src_ip[16];
	byte dst_ip[16];
}IPv6Header;

typedef struct _UdpHeader
{
	u_short src_port;
	u_short dst_port;
	u_short length;
	u_short sum;
}UdpHeader;

typedef struct _TcpHeader
{
	u_short src_port;
	u_short dst_port;
	unsigned int seq_num;
	unsigned int ack_num;
	byte hlen_resv;
	byte flags;
	u_short wnd_size;
	u_short sum;
	u_short up;
}TcpHeader;

typedef struct _adapterItem
{
	PWCHAR friendlyName;
	PCHAR adapterName;
	DWORD gatewayFlag;
	std::vector<SOCKET_ADDRESS> ipList;
}AdapterItem;

DWORD GetPidFromTcp(USHORT port);	//根据TCP端口查找PID
DWORD GetPidFromTcp6(USHORT port);	
DWORD GetPidFromUdp(USHORT port);	//根据UDP端口查找PID
DWORD GetPidFromUdp6(USHORT port);
char* GetProcessNameByPid(DWORD pid);


std::vector<AdapterItem> GetAdapterList(int*);
