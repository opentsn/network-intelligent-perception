#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <time.h>

FILE* OpenLogFile(const char*, const char*);
int WriteLog(FILE*, const char*, ...);
int CloseLogFile(FILE* logFile);

