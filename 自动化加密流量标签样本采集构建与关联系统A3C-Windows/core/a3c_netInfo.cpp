#include "a3c_netInfo.h"


DWORD GetPidFromTcp(USHORT port)
{
	PMIB_TCPTABLE_OWNER_PID pTcpTable = NULL;
	DWORD tableSize = 0;
	DWORD status = 0;
	DWORD itemCnt = 0;
	DWORD pid = 0;
	
	status = GetExtendedTcpTable(pTcpTable, &tableSize, 0, AF_INET, TCP_TABLE_OWNER_PID_ALL, 0);
	if (status != ERROR_INSUFFICIENT_BUFFER) 
	{
		//printf("Get pid from tcp error:%d\n", status);
		return UNKNOWN_ERROR;
	}

	pTcpTable = (PMIB_TCPTABLE_OWNER_PID)malloc(tableSize);
	if (pTcpTable == NULL)
	{
		//printf("Malloc pTcpTable error\n");
		return UNKNOWN_ERROR;
	}

	status = GetExtendedTcpTable(pTcpTable, &tableSize, 0, AF_INET, TCP_TABLE_OWNER_PID_ALL, 0);
	if (status != NO_ERROR)
	{
		//printf("Get pid from tcp error:%d\n", status);
		free(pTcpTable);
		return UNKNOWN_ERROR;
	}

	itemCnt = pTcpTable->dwNumEntries;
	for (unsigned int i = 0; i < itemCnt; i++)
	{
		//printf("every port:%d\n", ntohs(((PMIB_UDPTABLE_OWNER_PID)pUdpTable)->table[i].dwLocalPort));
		if (port == ntohs(pTcpTable->table[i].dwLocalPort))
		{
			pid = pTcpTable->table[i].dwOwningPid;
			break;
		}
	}

	if (pTcpTable)
		free(pTcpTable);

	return pid;
}

DWORD GetPidFromTcp6(USHORT port)
{
	PMIB_TCP6TABLE_OWNER_PID pTcpTable = NULL;
	DWORD tableSize = 0;
	DWORD status = 0;
	DWORD itemCnt = 0;
	DWORD pid = 0;

	status = GetExtendedTcpTable(pTcpTable, &tableSize, 0, AF_INET6, TCP_TABLE_OWNER_PID_ALL, 0);
	if (status != ERROR_INSUFFICIENT_BUFFER)
	{
		//printf("Get pid from tcp error:%d\n", status);
		return UNKNOWN_ERROR;
	}

	pTcpTable = (PMIB_TCP6TABLE_OWNER_PID)malloc(tableSize);
	if (pTcpTable == NULL)
	{
		//printf("Malloc pTcpTable error\n");
		return UNKNOWN_ERROR;
	}

	status = GetExtendedTcpTable(pTcpTable, &tableSize, 0, AF_INET6, TCP_TABLE_OWNER_PID_ALL, 0);
	if (status != NO_ERROR)
	{
		//printf("Get pid from tcp error:%d\n", status);
		free(pTcpTable);
		return UNKNOWN_ERROR;
	}

	itemCnt = pTcpTable->dwNumEntries;
	for (int i = 0; i < itemCnt; i++)
	{
		//printf("every port:%d\n", ntohs(((PMIB_UDPTABLE_OWNER_PID)pUdpTable)->table[i].dwLocalPort));
		if (port == ntohs(pTcpTable->table[i].dwLocalPort))
		{
			pid = pTcpTable->table[i].dwOwningPid;
			break;
		}
	}

	if (pTcpTable)
		free(pTcpTable);

	return pid;
}

DWORD GetPidFromUdp(USHORT port)
{
	PMIB_UDPTABLE_OWNER_PID pUdpTable = NULL;
	DWORD tableSize = 0;
	DWORD status = 0;
	DWORD itemCnt = 0;
	DWORD pid = 0;
	DWORD find_cnt = 0;

	status = GetExtendedUdpTable(pUdpTable, &tableSize, 0, AF_INET, UDP_TABLE_OWNER_PID, 0);
	if (status != ERROR_INSUFFICIENT_BUFFER)
	{
		//printf("Get pid from udp error:%d\n", status);
		return UNKNOWN_ERROR;
	}

	pUdpTable = (PMIB_UDPTABLE_OWNER_PID)malloc(tableSize);
	if (pUdpTable == NULL)
	{
		//printf("Malloc pUdpTable error\n");
		return UNKNOWN_ERROR;
	}

	status = GetExtendedUdpTable(pUdpTable, &tableSize, 0, AF_INET, UDP_TABLE_OWNER_PID, 0);
	if (status != NO_ERROR)
	{
		//printf("Get pid from udp error:%d\n", status);
		free(pUdpTable);
		return UNKNOWN_ERROR;
	}

	itemCnt = pUdpTable->dwNumEntries;
	for (int i = 0; i < itemCnt; i++)
	{
		//printf("every port:%d\n", ntohs(((PMIB_UDPTABLE_OWNER_PID)pUdpTable)->table[i].dwLocalPort));
		if (port == ntohs(pUdpTable->table[i].dwLocalPort))
		{
			pid = pUdpTable->table[i].dwOwningPid;
			find_cnt += 1;		//对于UDP端口，有可能一个端口被多个PID使用，归类为unknown pid
		}
	}

	if (pUdpTable)
		free(pUdpTable);

	if (find_cnt > 1)
		pid = UNKNOWN_PID;

	return pid;
}

DWORD GetPidFromUdp6(USHORT port)
{
	PMIB_UDP6TABLE_OWNER_PID pUdpTable = NULL;
	DWORD tableSize = 0;
	DWORD status = 0;
	DWORD itemCnt = 0;
	DWORD pid = 0;
	DWORD find_cnt = 0;

	status = GetExtendedUdpTable(pUdpTable, &tableSize, 0, AF_INET6, UDP_TABLE_OWNER_PID, 0);
	if (status != ERROR_INSUFFICIENT_BUFFER)
	{
		//printf("Get pid from udp6 error:%d\n", status);
		return UNKNOWN_ERROR;
	}

	pUdpTable = (PMIB_UDP6TABLE_OWNER_PID)malloc(tableSize);
	if (pUdpTable == NULL)
	{
		//printf("Malloc pUdpTable error\n");
		return UNKNOWN_ERROR;
	}

	status = GetExtendedUdpTable(pUdpTable, &tableSize, 0, AF_INET6, UDP_TABLE_OWNER_PID, 0);
	if (status != NO_ERROR)
	{
		//printf("Get pid from udp6 error:%d\n", status);
		free(pUdpTable);
		return UNKNOWN_ERROR;
	}

	itemCnt = pUdpTable->dwNumEntries;
	for (int i = 0; i < itemCnt; i++)
	{
		//printf("every port:%d\n", ntohs(((PMIB_UDPTABLE_OWNER_PID)pUdpTable)->table[i].dwLocalPort));
		if (port == ntohs(pUdpTable->table[i].dwLocalPort))
		{
			pid = pUdpTable->table[i].dwOwningPid;
			find_cnt += 1;		//对于UDP端口，有可能一个端口被多个PID使用，归类为unknown pid
		}
	}

	if (pUdpTable)
		free(pUdpTable);

	if (find_cnt > 1)
		pid = UNKNOWN_PID;

	return pid;
}

char* GetProcessNameByPid(DWORD pid)
{
	HANDLE hProcessSnap;
	PROCESSENTRY32 pe32;
	char* res = NULL;
	int len = 0;

	hProcessSnap = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	if (hProcessSnap == INVALID_HANDLE_VALUE)
	{
		//printf("CreateToolhelp32Snapshot error\n");
		return NULL;
	}

	pe32.dwSize = sizeof(PROCESSENTRY32);

	if (!Process32First(hProcessSnap, &pe32))
	{
		//printf("Process32First error\n");
		CloseHandle(hProcessSnap);
		return NULL;
	}

	do
	{
		if (pid == pe32.th32ProcessID)
		{
			len = WideCharToMultiByte(CP_ACP, 0, pe32.szExeFile, wcslen(pe32.szExeFile), NULL, 0, NULL, NULL);
			res = (char*)malloc(len + 6);
			if (res == NULL)
			{
				//printf("Malloc res error\n");
				break;
			}
			WideCharToMultiByte(CP_ACP, 0, pe32.szExeFile, wcslen(pe32.szExeFile), res, len, NULL, NULL);
			res[len] = '\0';
			break;
		}
	} while (Process32Next(hProcessSnap, &pe32));

	CloseHandle(hProcessSnap);
	return res;
}

std::vector<AdapterItem> GetAdapterList(int *cnt)
{
	std::vector<AdapterItem> resList;
	AdapterItem item;
	DWORD dwStatus = 0;
	PIP_ADAPTER_ADDRESSES pAdapterAddr = NULL;
	PIP_ADAPTER_ADDRESSES pCurrent = NULL;
	PIP_ADAPTER_UNICAST_ADDRESS pUnicastAddr = NULL;
	ULONG outBufLen = sizeof(IP_ADAPTER_ADDRESSES);

	pAdapterAddr = (PIP_ADAPTER_ADDRESSES)malloc(outBufLen);
	if (pAdapterAddr == NULL)
	{
		//printf("Memory allocation failed for pAdapterAddr\n");
		return resList;
	}
	dwStatus = GetAdaptersAddresses(AF_UNSPEC, GAA_FLAG_INCLUDE_PREFIX, NULL, pAdapterAddr, &outBufLen);

	if (dwStatus == ERROR_BUFFER_OVERFLOW)
	{
		free(pAdapterAddr);
		pAdapterAddr = (PIP_ADAPTER_ADDRESSES)malloc(outBufLen);
		if (pAdapterAddr == NULL)
		{
			//printf("Memory allocation failed for pAdapterAddr\n");
			return resList;
		}
		dwStatus = GetAdaptersAddresses(
			AF_UNSPEC, GAA_FLAG_INCLUDE_PREFIX | GAA_FLAG_INCLUDE_GATEWAYS, NULL, pAdapterAddr, &outBufLen);
	}
	
	if (dwStatus == NO_ERROR)
	{
		pCurrent = pAdapterAddr;
		while (pCurrent)
		{
			item.adapterName = pCurrent->AdapterName;
			item.friendlyName = pCurrent->FriendlyName;
			item.gatewayFlag = pCurrent->FirstGatewayAddress == NULL ? 0 : 1;
			pUnicastAddr = pCurrent->FirstUnicastAddress;
			while (pUnicastAddr)
			{
				SOCKET_ADDRESS socketAddr = pUnicastAddr->Address;
				item.ipList.push_back(socketAddr);
				pUnicastAddr = pUnicastAddr->Next;
			}
			resList.push_back(item);
			pCurrent = pCurrent->Next;
			*cnt += 1;
		}
	}
	else
	{
		//printf("Failed when calling GetAdaptersAddresses\n");
	}
	return resList;
}
