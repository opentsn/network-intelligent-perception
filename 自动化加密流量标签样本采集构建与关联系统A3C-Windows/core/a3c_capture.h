#pragma once
#include "a3c_log.h"
#include "a3c_netInfo.h"
#include <pcap.h>
#include <unordered_map>
#include <vector>

#pragma comment(lib, "Packet.lib")
#pragma comment(lib, "wpcap.lib")

using namespace std;

//typedef void (_stdcall *PInvokeType)(const char*);

class CaptureHelper
{
public:
	static void CallBack(u_char*, const struct pcap_pkthdr*, const u_char*);
	BOOL LoadNpcapDlls();
	DWORD Init();
	DWORD SelectAdapter();
	DWORD StartCapture(PCHAR adapterName);
	DWORD CleanUp();
	void packet_handler(u_char*, const struct pcap_pkthdr*, const u_char*);
	void GetTransportInfo(const u_char* pkt_data, u_short* port, u_short*, u_short*);
	void BreakLoop();

	//PInvokeType outputFun;
	int adpCnt;
	
	AdapterItem selAdapter;
	vector<AdapterItem> adapterList;
	string dirName;
	FILE* logFile;
	static CaptureHelper* CurObj;
	unordered_map<u_short, string> tcp_to_pro;
	unordered_map<u_short, string> udp_to_pro;
	unordered_map<string, u_char*> pro_to_dumper;

	pcap_t* adhandle = NULL;
	pcap_dumper_t* dumpfile_all;		//存储所有数据包
	pcap_dumper_t* dumpfile_unknown;	//存储未知进程数据包
	pcap_dumper_t* dumpfile_other;		//存储其他数据包
	pcap_dumper_t* dumpfile_dns;        //存储所有DNS

	BOOL stopFlag;

	int pkt_cnt;	//包总数
	int pro_cnt;	//进程总数
	unordered_map<string, int> pro_to_cnt;	//进程抓包数量信息
	vector<string> proIndex;
};
int bytesCmp(UCHAR*, UCHAR*, int);