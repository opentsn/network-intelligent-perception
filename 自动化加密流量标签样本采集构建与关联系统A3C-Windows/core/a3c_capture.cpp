#include "a3c_capture.h"

int bytesCmp(UCHAR* s1, UCHAR* s2, int length)
{
	for (int i = 0; i < length; i++)
	{
		if (s1[i] != s2[i])
			return -1;
	}
	return 0;
}

CaptureHelper* CaptureHelper::CurObj = NULL;

void CaptureHelper::CallBack(u_char* dumpfile, const struct pcap_pkthdr* header, const u_char* pkt_data)
{
	CurObj->packet_handler(dumpfile, header, pkt_data);
}

BOOL CaptureHelper::LoadNpcapDlls()
{
	_TCHAR npcap_dir[512];
	UINT len;
	len = GetSystemDirectory(npcap_dir, 480);
	if (!len) {
		//fprintf(stderr, "Error in GetSystemDirectory: %x", GetLastError());
		return FALSE;
	}
	_tcscat_s(npcap_dir, 512, _T("\\Npcap"));
	if (SetDllDirectory(npcap_dir) == 0) {
		//fprintf(stderr, "Error in SetDllDirectory: %x", GetLastError());
		return FALSE;
	}
	return TRUE;
}

DWORD CaptureHelper::Init()
{
	CurObj = this;
	adpCnt = 0;
	adapterList = GetAdapterList(&adpCnt);
	return 0;
}
//返回自动选择的网卡在adapterList中的索引
DWORD CaptureHelper::SelectAdapter()
{
	DWORD res = 0;
	if (adapterList.empty())
	{
		//printf("No Adapter\n");
	}
	vector<AdapterItem>::iterator it;
	for (it = adapterList.begin(); it != adapterList.end(); it++)
	{
		//printf("in sel: %wS\n", it->friendlyName);
		if (it->gatewayFlag == 1)	//自动选择第一个有网关的适配器
		{
			selAdapter = *it;
			break;
		}
		res++;
	}
	return res;
}

DWORD CaptureHelper::StartCapture(PCHAR adapterName)
{
	if (adapterName == NULL)
	{
		WriteLog(logFile, "adapter is null\n");
		return -1;
	}
	//创建输出文件夹
	time_t t = time(NULL);
	char dir[64] = { 0 };
	strftime(dir, sizeof(dir), "%Y-%m-%d-%H-%M-%S", localtime(&t));
	dirName = dir;
	string cmd = "mkdir " + dirName;
	//system(cmd.c_str());
	CreateDirectoryA(dir, NULL);


	logFile = OpenLogFile(strcat(dir, "/log.txt"), "w");
	if (!LoadNpcapDlls())
	{
		WriteLog(logFile, "Couldn't load Npcap\n");
		return -1;
	}
	//停止信号置零
	stopFlag = FALSE;
	pkt_cnt = 0;
	pro_cnt = 0;

	char devName[55] = "\\Device\\NPF_";
	char errbuf[PCAP_ERRBUF_SIZE];

	//整成形如：\Device\NPF_{2A0EB218-9B96-4CE5-9EB2-5510FCD83624}
	strcat_s(devName, 55, adapterName);

	adhandle = pcap_create(devName, errbuf);
	if (adhandle == NULL)
	{
		WriteLog(logFile, "Unable to open the adapter. %s is not supported by Npcap\n", adapterName);
		return -1;
	}

	int status = pcap_set_buffer_size(adhandle, 200 * 1024 * 1024);
	if (status != 0)
	{
		WriteLog(logFile, "pcap set buffer size error\n");
		return -1;
	}
	status = pcap_set_timeout(adhandle, 100);
	if (status != 0)
	{
		WriteLog(logFile, "pcap set timeout error\n");
		return -1;
	}
	status = pcap_activate(adhandle);
	if (status != 0)
	{
		WriteLog(logFile, "pcap activate error. devName: %s\n", devName);
		return -1;
	}
	WriteLog(logFile, "pcap active success on %s\n", devName);
	dumpfile_all = pcap_dump_open(adhandle, (dirName + "/all.pcap").c_str());
	dumpfile_unknown = pcap_dump_open(adhandle, (dirName + "/unknown.pcap").c_str());
	dumpfile_other = pcap_dump_open(adhandle, (dirName + "/other.pcap").c_str());
	dumpfile_dns = pcap_dump_open(adhandle, (dirName + "/DNSPro.pcap").c_str());

	if (dumpfile_all == NULL || dumpfile_unknown == NULL || dumpfile_other == NULL || dumpfile_dns == NULL)
	{
		WriteLog(logFile, "Error opening output file\n");
		return -1;
	}

	//printf("\nlistening on %s... Press Ctrl+C to stop...\n", adapterName);
	//outputFun("\nlistening on. Press Ctrl+C to stop...\n");
	pcap_loop(adhandle, 0, CallBack, (unsigned char*)dumpfile_all);

	return 0;
}

void CaptureHelper::packet_handler(u_char* dumpfile, const struct pcap_pkthdr* header, const u_char* pkt_data)
{
	//printf("pkt cnt: %d\n", ++test_cnt);
	pkt_cnt++;
	
	if (stopFlag == TRUE)
	{
		pcap_breakloop(adhandle);
		return;
	}
	pcap_dump(dumpfile, header, pkt_data);

	/*char tmp[15];
	_snprintf_s(tmp, 15, 15, "pkt cnt:%d\n", ++pkt_cnt);*/
	//outputFun(tmp);

	/*struct tm ltime;
	char timestr[16];
	time_t local_tv_sec;*/
	u_short port = 0;
	u_short ip_protocol = 0;
	u_short trans_protocol = 0;
	DWORD pid = 0;

	GetTransportInfo(pkt_data, &port, &ip_protocol, &trans_protocol);

	if (trans_protocol == TYPE_TCP)	//tcp
	{
		unordered_map<u_short, string>::iterator pro_it = tcp_to_pro.find(port);
		if (pro_it != tcp_to_pro.end())	//1.直接在端口进程映射表里检索到
		{
			u_char* dumper = pro_to_dumper[pro_it->second];
			pcap_dump(dumper, header, pkt_data);
			pro_to_cnt[pro_it->second] += 1;
			WriteLog(logFile, "pcap_file 1:\n");
			WriteLog(logFile, "%s\n", tcp_to_pro[port].c_str());
			WriteLog(logFile, "tcp port: %d\n", port);
			return;
		}

		if (ip_protocol == TYPE_IP)
		{
			pid = GetPidFromTcp(port);
		}
		else //IPv6
		{
			pid = GetPidFromTcp6(port);
		}

		if (pid == 0) //pid=0是暂时不能确定的进程，先加入到unknow里
		{
			pcap_dump((u_char*)dumpfile_unknown, header, pkt_data);
			WriteLog(logFile, "pcap_file pid=0:\n");
			WriteLog(logFile, "unknown\n");
			WriteLog(logFile, "tcp port: %d\n", port);
			return;
		}

		string process_name = GetProcessNameByPid(pid);
		if (process_name.empty())
		{
			//printf("Get process name error\n");
			WriteLog(logFile, "empty !!!!!!!!!!!!!!!!!!!!!!!!\n");
			return;
		}

		process_name += ".pcap";

		unordered_map<string, u_char*>::iterator dumper_it = pro_to_dumper.find(process_name);
		if (dumper_it != pro_to_dumper.end())	//2.在现有进程中检索到
		{
			pcap_dump(dumper_it->second, header, pkt_data);
			pair<u_short, string>tmp(port, process_name);
			tcp_to_pro.insert(tmp);
			pro_to_cnt[process_name] += 1;
			WriteLog(logFile, "pcap_file 2 PID:%d\n", pid);
			WriteLog(logFile, "%s\n", tcp_to_pro[port].c_str());
			WriteLog(logFile, "tcp port: %d\n", port);
			return;
		}
		//3.此流量属于新的进程
		pro_cnt++;
		pcap_dumper_t* df = pcap_dump_open(adhandle, (dirName + "/" + process_name).c_str());

		pcap_dump((u_char*)df, header, pkt_data);
		pair<u_short, string> tmp1(port, process_name);
		tcp_to_pro.insert(tmp1);
		pair<string, u_char*> tmp2(process_name, (u_char*)df);
		pro_to_dumper.insert(tmp2);
		pair<string, int> tmp3(process_name, 1);
		pro_to_cnt.insert(tmp3);
		proIndex.push_back(process_name);

		WriteLog(logFile, "pcap_file 3:\n");
		WriteLog(logFile, "%s\n", tcp_to_pro[port].c_str());
		WriteLog(logFile, "tcp port: %d\n", port);
	}
	else if (trans_protocol == TYPE_UDP)//UDP
	{
		unordered_map<u_short, string>::iterator pro_it = udp_to_pro.find(port);
		if (pro_it != udp_to_pro.end())
		{
			u_char* dumper = pro_to_dumper[pro_it->second];
			pcap_dump(dumper, header, pkt_data);
			pro_to_cnt[pro_it->second] += 1;
			WriteLog(logFile, "pcap_file 1:\n");
			WriteLog(logFile, "%s\n", udp_to_pro[port].c_str());
			WriteLog(logFile, "udp port: %d\n", port);
			return;
		}

		if (ip_protocol == TYPE_IP)
		{
			pid = GetPidFromUdp(port);
		}
		else //IPv6
		{
			pid = GetPidFromUdp6(port);
		}
		//UDP多了这一步，一个端口被多个进程使用时也在这里处理
		if (pid == UNKNOWN_PID)
		{
			pcap_dump((u_char*)dumpfile_other, header, pkt_data);
			WriteLog(logFile, "pcap_file:\n");
			WriteLog(logFile, "other\n");
			WriteLog(logFile, "udp port: %d\n", port);
			return;
		}

		if (pid == 0) //实际上这就是暂时不能确定的进程，先加入到unknow里，后面离线进一步关联
		{
			pcap_dump((u_char*)dumpfile_unknown, header, pkt_data);
			WriteLog(logFile, "pcap_file pid=0:\n");
			WriteLog(logFile, "unknown\n");
			WriteLog(logFile, "udp port: %d\n", port);
			return;
		}

		string process_name = GetProcessNameByPid(pid);
		if (process_name.empty())
		{
			//printf("Get process name error\n");
			WriteLog(logFile, "empty !!!!!!!!!!!!!!!!!!!!!!!\n");
			return;
		}

		process_name += ".pcap";
		unordered_map<string, u_char*>::iterator dumper_it = pro_to_dumper.find(process_name);
		if (dumper_it != pro_to_dumper.end())
		{
			pcap_dump(dumper_it->second, header, pkt_data);
			pair<u_short, string>tmp(port, process_name);
			udp_to_pro.insert(tmp);
			pro_to_cnt[process_name] += 1;

			WriteLog(logFile, "pcap_file 2 PID: %d\n", pid);
			WriteLog(logFile, "%s\n", udp_to_pro[port].c_str());
			WriteLog(logFile, "udp port: %d\n", port);
			return;
		}

		pcap_dumper_t* df = pcap_dump_open(adhandle, (dirName + "/" + process_name).c_str());

		pcap_dump((u_char*)df, header, pkt_data);
		pair<u_short, string> tmp1(port, process_name);
		udp_to_pro.insert(tmp1);
		pair<string, u_char*> tmp2(process_name, (u_char*)df);
		pro_to_dumper.insert(tmp2);
		pair<string, int> tmp3(process_name, 1);
		pro_to_cnt.insert(tmp3);
		proIndex.push_back(process_name);

		WriteLog(logFile, "pcap_file 3:\n");
		WriteLog(logFile, "%s\n", udp_to_pro[port].c_str());
		WriteLog(logFile, "udp port: %d\n", port);
	}
	else if (trans_protocol == TYPE_DNS)
	{
		pcap_dump((u_char*)dumpfile_dns, header, pkt_data);
		WriteLog(logFile, "pcap_file:\n");
		WriteLog(logFile, "DNSPro\n");
		WriteLog(logFile, "port: %d\n", port);
	}
	else
	{
		pcap_dump((u_char*)dumpfile_other, header, pkt_data);
		WriteLog(logFile, "pcap_file:\n");
		WriteLog(logFile, "other\n");
		WriteLog(logFile, "port: %d\n", port);
		return;
	}
}

void CaptureHelper::BreakLoop()
{
	stopFlag = TRUE;
}

DWORD CaptureHelper::CleanUp()
{
	//关闭适配器句柄，关闭四个特殊的pcap文件，日志文件
	pcap_close(adhandle);
	pcap_dump_close(dumpfile_all);
	pcap_dump_close(dumpfile_dns);
	pcap_dump_close(dumpfile_unknown);
	pcap_dump_close(dumpfile_other);
	CloseLogFile(logFile);
	dumpfile_all = NULL;
	dumpfile_dns = NULL;
	dumpfile_other = NULL;
	dumpfile_unknown = NULL;
	adhandle = NULL;
	logFile = NULL;
	//遍历关闭所有打开过的文件
	unordered_map<string, u_char*>::iterator it;
	for (it = pro_to_dumper.begin(); it != pro_to_dumper.end(); it++)
	{
		pcap_dump_close((pcap_dumper_t*)it->second);
	}
	tcp_to_pro.clear();
	udp_to_pro.clear();
	pro_to_dumper.clear();
	pro_to_cnt.clear();
	proIndex.clear();
	return 0;
}

void CaptureHelper::GetTransportInfo(const u_char* pkt_data, u_short* port, u_short* ip_protocol, u_short* trans_protocol)
{
	u_short src_port;
	u_short dst_port;
	u_short recvFlag = 0;	//为0表示发出去的包
	vector<SOCKET_ADDRESS>::iterator it;
	sockaddr_in* sin = NULL;
	sockaddr_in6* sin6 = NULL;
	EthHeader* eh = NULL;
	eh = (EthHeader*)pkt_data;
	*ip_protocol = eh->type;

	if (eh->type == TYPE_IP)
	{
		IPHeader* ih = NULL;
		ih = (IPHeader*)(pkt_data + 14);
		//获取IP地址，判断包方向
		for (it = selAdapter.ipList.begin(); it != selAdapter.ipList.end(); it++)
		{
			if (it->lpSockaddr->sa_family != AF_INET)
				continue;
			sin = (sockaddr_in*)it->lpSockaddr;
			//0xf5d9c90a
			if (sin->sin_addr.S_un.S_addr == ih->dst_ip)
			{
				recvFlag = 1;
				break;
			}
		}
		//获取传输层协议，分开处理TCP和UDP
		*trans_protocol = ih->protocol;
		if (ih->protocol == TYPE_TCP)
		{
			TcpHeader* th = NULL;
			th = (TcpHeader*)((u_char*)ih + (long long)(ih->ver_hdlen & 0x0F) * 4);
			src_port = ntohs(th->src_port);
			dst_port = ntohs(th->dst_port);
			*port = recvFlag == 0 ? src_port : dst_port;
		}
		else if (ih->protocol == TYPE_UDP)
		{
			UdpHeader* uh = NULL;
			uh = (UdpHeader*)((u_char*)ih + (long long)(ih->ver_hdlen & 0x0F) * 4);
			src_port = ntohs(uh->src_port);
			dst_port = ntohs(uh->dst_port);
			*port = recvFlag == 0 ? src_port : dst_port;
			if (src_port == 53 || dst_port == 53)	//针对DNS的特殊处理
			{
				*trans_protocol = TYPE_DNS;
			}
		}
		else
		{
			//printf("Unknown protocol at transport layer\n");
		}
	}
	else if (eh->type == TYPE_IP6)
	{
		IPv6Header* ih = NULL;
		ih = (IPv6Header*)(pkt_data + 14);
		//获取IP地址，判断包方向
		for (it = selAdapter.ipList.begin(); it != selAdapter.ipList.end(); it++)
		{
			if (it->lpSockaddr->sa_family != AF_INET6)
				continue;
			sin6 = (sockaddr_in6*)it->lpSockaddr;
			
			if (bytesCmp(sin6->sin6_addr.u.Byte, ih->dst_ip, 16) == 0)
			{
				recvFlag = 1;
				break;
			}
		}
		//获取传输层协议，分开处理TCP和UDP
		*trans_protocol = ih->next_header;
		if (ih->next_header == TYPE_TCP)
		{
			TcpHeader* th = NULL;
			th = (TcpHeader*)((u_char*)ih + 40);
			src_port = ntohs(th->src_port);
			dst_port = ntohs(th->dst_port);
			*port = recvFlag == 0 ? src_port : dst_port;
		}
		else if (ih->next_header == TYPE_UDP)
		{
			UdpHeader* uh = NULL;
			uh = (UdpHeader*)((u_char*)ih + 40);
			src_port = ntohs(uh->src_port);
			dst_port = ntohs(uh->dst_port);
			*port = recvFlag == 0 ? src_port : dst_port;
			if (src_port == 53 || dst_port == 53)	//针对DNS的特殊处理
			{
				*trans_protocol = TYPE_DNS;
			}
		}
		else
		{
			//printf("Unknown protocol at transport layer\n");
		}
	}
	else
	{
		//printf("Unknown protocol at ip layer\n");
	}
}
